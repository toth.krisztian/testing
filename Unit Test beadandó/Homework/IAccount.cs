﻿    using Homework.ThirdParty;

namespace Homework
{
    // Never used -> delete
    public interface IAccount
    {
        int ActionsSuccessfullyPerformed { get; }
        int Id { get; }
        bool IsConfirmed { get; }
        bool IsRegistered { get; }

        void Activate();
        void Register();
        bool TakeAction(IAction action);
    }
}