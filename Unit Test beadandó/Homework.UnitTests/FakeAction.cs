﻿using Homework.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework.UnitTests
{
    class FakeAction : IAction
    {
        public int counter = 0;
        public bool Execute()
        {
            counter++;
            return true;
        }
    }
}
