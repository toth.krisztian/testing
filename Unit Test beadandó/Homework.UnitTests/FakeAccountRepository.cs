﻿using Homework.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework.UnitTests
{
    class FakeAccountRepository : IAccountRepository
    {
        private List<Account> _accounts = new List<Account>();


        public bool Add(Account account)
        {
            bool success = false;
            if(!Exists(account.Id))
            {
                _accounts.Add(account);
                success = true;
            }
            return success;
        }

        public bool Exists(int accountId)
        {
            bool exists = false;
            if(_accounts.Any(acc => acc.Id == accountId))
            {
                exists = true;
            }
            return exists;
        }

        public Account Get(int accountId)
        {
            return _accounts.FirstOrDefault(acc => acc.Id == accountId);
        }

        public IEnumerable<Account> GetAll()
        {
            return _accounts.ToList();
        }

        public bool Remove(int accountId)
        {
            bool success = false;
            if(Exists(accountId))
            {
                _accounts.Remove(Get(accountId));
                success = true;
            }
            return success;
        }
    }
}
