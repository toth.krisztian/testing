﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using System.Reflection;

namespace Homework.UnitTests
{
    [TestFixture]
    
    public class HomeworkTests
    {
        Random rnd = new Random();
        private Account _zeroAccount;
        private Account _ttAccount;
        private Account _tfAccount;
        private Account _ftAccount;
        private Mock<IAction> _mockIAction;
        private IAccountRepository _accountRepository;
        private AccountActivityService accountActivityService;
        private FakeAction _fakeAction;

        [SetUp]
        public void SetUp()
        {
            _mockIAction = new Mock<IAction>();
            _accountRepository = new FakeAccountRepository();
            _fakeAction = new FakeAction();
            _zeroAccount = new Account(0);
            _ttAccount = new Account(0);
            _ttAccount.Register();
            _ttAccount.Activate();
            _tfAccount = new Account(0);
            _tfAccount.Register();
            _ftAccount = new Account(0);
            _ftAccount.Activate();
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(5)]
        [TestCase(null)]
        [TestCase("3")]
        [TestCase(int.MinValue)]
        [TestCase(int.MaxValue)]
        [Category("AccountTests")]
        public void Account_CTOR_Test(int id)
        {
            Account _account = new Account(id);

            Assert.That(_account.Id, Is.EqualTo(id));
            Assert.That(_account.IsRegistered, Is.EqualTo(false));
            Assert.That(_account.IsConfirmed, Is.EqualTo(false));
            Assert.That(_account.ActionsSuccessfullyPerformed, Is.EqualTo(0));
        }

        [Test]
        [Category("AccountTests")]
        public void Account_Register_Test()
        {
            _zeroAccount.Register();

            Assert.That(_zeroAccount.IsRegistered, Is.EqualTo(true));
        }

        [Test]
        [Category("AccountMethodTests")]
        public void Account_Activate_Test()
        {
            _zeroAccount.Activate();

            Assert.That(_zeroAccount.IsConfirmed, Is.EqualTo(true));
        }

        //[Test]
        //[Category("AccountPrivateMethodTests")]
        // Testing private methods in the Account Class.
        //public void Account_IsNotRegistered_Test()
        //{
        //    Account _account = new Account(0);
        //    _account.Register();
        //    MethodInfo methodInfo = typeof(Account).GetMethod("IsNotRegistered", BindingFlags.NonPublic | BindingFlags.Instance);
        //    object[] parameters = { };
        //
        //    methodInfo.Invoke(_account, parameters);
        //
        //    Assert.That(_account.IsRegistered, Is.EqualTo(true));
        //}

        //[Test]
        //[Category("AccountPrivateMethodTests")]
        // Testing private methods in the Account Class.
        //public void Account_IsNotRegistered_Test()
        //{
        //    Account _account = new Account(0);
        //    _account.Register();
        //    MethodInfo methodInfo = typeof(Account).GetMethod("IsNotConfirmed", BindingFlags.NonPublic | BindingFlags.Instance);
        //    object[] parameters = { };
        //
        //    methodInfo.Invoke(_account, parameters);
        //
        //    Assert.That(_account.IsConfirmed, Is.EqualTo(true));
        //}

        [Test]
        [Category("AccountMethodTests")]
        // IsRegistered = false, IsConfirmed = false;
        public void Account_TakeAction_IsInactiveAccount_FF_Test()
        {
            _mockIAction = new Mock<IAction>();
            IAction action = _mockIAction.Object;

            Assert.That(() => _zeroAccount.TakeAction(action), Throws.TypeOf<InactiveUserException>());
            _mockIAction.Verify(m => m.Execute(), Times.Never);
        }

        // Replaced with mock version
        //[TestCase()]
        //[Category("AccountMethodTests")]
        //// IsRegistered = true, IsConfirmed = true;
        //// Using hte FakeAction Class
        //public void Account_TakeAction_PerformAction_TT_Fake_Test()
        //{
        //    FakeAction _fakeAction = new FakeAction();
        //
        //    Assert.That(() => _ttAccount.TakeAction(_fakeAction), Is.EqualTo(true));
        //    Assert.That(() => _ttAccount.ActionsSuccessfullyPerformed, Is.EqualTo(1));
        //}

        [TestCase()]
        [Category("AccountMethodTests")]
        // IsRegistered = true, IsConfirmed = false;
        // Using hte FakeAction Class
        public void Account_TakeAction_PerformAction_TF_Test()
        {
            Assert.That(() => _tfAccount.TakeAction(_fakeAction), Is.EqualTo(true));
            Assert.That(() => _tfAccount.ActionsSuccessfullyPerformed, Is.EqualTo(1));
        }

        // Replaced with mock version
        //[TestCase()]
        //[Category("AccountMethodTests")]
        //// IsRegistered = false, IsConfirmed = true;
        //// Using hte FakeAction Class
        //public void Account_TakeAction_PerformAction_FT_Test()
        //{
        //    FakeAction _fakeAction = new FakeAction();
        //
        //    Assert.That(() => _ftAccount.TakeAction(_fakeAction), Is.EqualTo(true));
        //    Assert.That(() => _ftAccount.ActionsSuccessfullyPerformed, Is.EqualTo(1));
        //}

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(15)]
        //[TestCase(null)]
        //[TestCase("8")]
        [Category("AccountMethodTests")]
        // IsRegistered = false, IsConfirmed = true;
        // Using the FakeAction Class
        public void Account_TakeAction_PerformAction_MultiplePerformAction_Test(int ind)
        {
            for (int i = 0; i < ind; i++)
            {
                _ttAccount.TakeAction(_fakeAction);
            }

            Assert.That(_ttAccount.ActionsSuccessfullyPerformed, Is.EqualTo(ind));
            Assert.That(_fakeAction.counter, Is.EqualTo(ind));
        }

        [Test]
        [Category("AccountMethodTests")]
        // IsRegistered = true, IsConfirmed = true;
        // Using Mock.
        public void Account_TakeAction_PerformAction_TT_Mock_Test()
        {
            _mockIAction.Setup(m => m.Execute()).Returns(true);
            IAction _fakeAction = _mockIAction.Object;

            Assert.That(() => _ttAccount.TakeAction(_fakeAction), Is.EqualTo(true));
            Assert.That(() => _ttAccount.ActionsSuccessfullyPerformed, Is.EqualTo(1));
            _mockIAction.Verify(m => m.Execute(), Times.Once);
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(15)]
        [Category("AccountMethodTests")]
        // IsRegistered = false, IsConfirmed = true;
        // Using Mock.
        public void Account_TakeAction_PerformAction_MultiplePerformAction_True_Mock_Test(int ind)
        {
            _mockIAction.Setup(m => m.Execute()).Returns(true);
            IAction _fakeAction = _mockIAction.Object;

            for (int i = 0; i < ind; i++)
            {
                _ttAccount.TakeAction(_fakeAction);
            }

            Assert.That(_ttAccount.ActionsSuccessfullyPerformed, Is.EqualTo(ind));
            _mockIAction.Verify(m => m.Execute(), Times.Exactly(ind));
        }

        [TestCase(5)]
        [Category("AccountMethodTests")]
        // IsRegistered = false, IsConfirmed = true;
        // Using Mock.
        public void Account_TakeAction_PerformAction_MultiplePerformAction_False_Mock_Test(int ind)
        {
            _mockIAction.Setup(m => m.Execute()).Returns(false);
            IAction _fakeAction = _mockIAction.Object;

            for (int i = 0; i < ind; i++)
            {
                _ttAccount.TakeAction(_fakeAction);
            }

            Assert.That(_ttAccount.ActionsSuccessfullyPerformed, Is.EqualTo(0));
            _mockIAction.Verify(m => m.Execute(), Times.Exactly(ind));
        }

        [Test]
        [Category("AccountActivityServiceMethodTests")]
        public void AccountActivityService_GetActivity_AccountNotExistsException_Thrown_Test()
        {
            Mock<IAccountRepository> _mockIAccountRepository;
            _mockIAccountRepository = new Mock<IAccountRepository>();
            IAccountRepository _accountRepository = _mockIAccountRepository.Object;    
            accountActivityService = new AccountActivityService(_accountRepository);

            Assert.That(() => accountActivityService.GetActivity(0), Throws.TypeOf<AccountNotExistsException>());
            _mockIAccountRepository.Verify(m => m.Get(It.IsAny<int>()), Times.Once);
            _mockIAccountRepository.Verify(m => m.Remove(It.IsAny<int>()), Times.Never);
            _mockIAccountRepository.Verify(m => m.Add(It.IsAny<Account>()), Times.Never);
            _mockIAccountRepository.Verify(m => m.Exists(It.IsAny<int>()), Times.Never);
        }

        [Test]
        [Category("AccountActivityServiceMethodTests")]
        public void AccountActivityService_GetActivity_ActivityLevel_None_Test()
        {
            Account _tempAccount = new Account(1);
            _accountRepository.Add(_tempAccount);
            accountActivityService = new AccountActivityService(_accountRepository);

            Assert.That(() => accountActivityService.GetActivity(1), Is.EqualTo(ActivityLevel.None));
        }

        [TestCase(1)]
        [TestCase(5)]
        [TestCase(19)]
        [Category("AccountActivityServiceMethodTests")]
        public void AccountActivityService_GetActivity_ActivityLevel_Low_Test(int ind)
        {
            _accountRepository.Add(_ttAccount);
            accountActivityService = new AccountActivityService(_accountRepository);
            IAction _fakeAction = _mockIAction.Object;
            _mockIAction.Setup(m => m.Execute()).Returns(true);
            for (int i = 0; i < ind; i++)
            {
                _ttAccount.TakeAction(_fakeAction);
            }

            Assert.That(() => accountActivityService.GetActivity(0), Is.EqualTo(ActivityLevel.Low));
            Assert.That(_ttAccount.ActionsSuccessfullyPerformed, Is.EqualTo(ind));
            _mockIAction.Verify(m => m.Execute(), Times.Exactly(ind));
        }

        [TestCase(20)]
        [TestCase(30)]
        [TestCase(39)]
        [Category("AccountActivityServiceMethodTests")]
        public void AccountActivityService_GetActivity_ActivityLevel_Medium_Test(int ind)
        {
            _accountRepository.Add(_ttAccount);
            accountActivityService = new AccountActivityService(_accountRepository);
            IAction _fakeAction = _mockIAction.Object;
            _mockIAction.Setup(m => m.Execute()).Returns(true);
            for (int i = 0; i < ind; i++)
            {
                _ttAccount.TakeAction(_fakeAction);
            }

            Assert.That(() => accountActivityService.GetActivity(0), Is.EqualTo(ActivityLevel.Medium));
            Assert.That(_ttAccount.ActionsSuccessfullyPerformed, Is.EqualTo(ind));
            _mockIAction.Verify(m => m.Execute(), Times.Exactly(ind));
        }

        [TestCase(40)]
        [TestCase(41)]
        [TestCase(60)]
        [Category("AccountActivityServiceMethodTests")]
        public void AccountActivityService_GetActivity_ActivityLevel_High_Test(int ind)
        {
            _accountRepository.Add(_ttAccount);
            accountActivityService = new AccountActivityService(_accountRepository);
            IAction _fakeAction = _mockIAction.Object;
            _mockIAction.Setup(m => m.Execute()).Returns(true);
            for (int i = 0; i < ind; i++)
            {
                _ttAccount.TakeAction(_fakeAction);
            }

            Assert.That(() => accountActivityService.GetActivity(0), Is.EqualTo(ActivityLevel.High));
            Assert.That(_ttAccount.ActionsSuccessfullyPerformed, Is.EqualTo(ind));
            _mockIAction.Verify(m => m.Execute(), Times.Exactly(ind));
        }

        [TestCase(1)]
        [TestCase(5)]
        [TestCase(12)]
        [Category("AccountActivityServiceMethodTests")]
        public void AccountActivityService_GetAmountForActivity_None_Test(int ind)
        {
            accountActivityService = new AccountActivityService(_accountRepository);
            for (int i = 0; i < ind; i++)
            {
                _accountRepository.Add(new Account(i));
            }

            accountActivityService.GetAmountForActivity(ActivityLevel.None);

            Assert.That(accountActivityService.GetAmountForActivity(ActivityLevel.None), Is.EqualTo(ind));
        }

        [TestCase(1, ActivityLevel.Low, 1, 20)]
        [TestCase(5, ActivityLevel.Low, 1, 20)]
        [TestCase(12, ActivityLevel.Low, 1, 20)]
        [TestCase(40, ActivityLevel.Low, 1, 20)]
        [TestCase(2, ActivityLevel.Medium, 20, 40)]
        [TestCase(7, ActivityLevel.Medium, 20, 40)]
        [TestCase(13, ActivityLevel.Medium, 20, 40)]
        [TestCase(63, ActivityLevel.Medium, 20, 40)]
        [TestCase(3, ActivityLevel.High, 40, 43)]
        [TestCase(8, ActivityLevel.High, 40, 56)]
        [TestCase(17, ActivityLevel.High, 40, 266)]
        [TestCase(57, ActivityLevel.High, 40, 121)]
        [Category("AccountActivityServiceMethodTests")]
        public void AccountActivityService_GetAmountForActivity_Low_Medium_High_Test(int ind, ActivityLevel level,int minnum, int maxnum)
        {
            IAction _fakeAction = _mockIAction.Object;
            _mockIAction.Setup(m => m.Execute()).Returns(true);
            accountActivityService = new AccountActivityService(_accountRepository);
            int counter = 0;
            for (int i = 0; i < ind; i++)
            {
                Account tempAccounnt = new Account(i);
                int mod = rnd.Next(0, 3);
                switch (mod)
                {
                    case 0:
                        tempAccounnt.Activate();
                        break;
                    case 1:
                        tempAccounnt.Register();
                        break;
                    case 2:
                        tempAccounnt.Activate();
                        tempAccounnt.Register();
                        break;
                }
                for (int j = 0; j < rnd.Next(minnum,maxnum); j++)
                {
                    tempAccounnt.TakeAction(_fakeAction);
                    counter++;
                }
                _accountRepository.Add(tempAccounnt);
            }

            accountActivityService.GetAmountForActivity(level);

            Assert.That(accountActivityService.GetAmountForActivity(level), Is.EqualTo(ind));
            _mockIAction.Verify(m => m.Execute(), Times.Exactly(counter));
        }
    }
}
