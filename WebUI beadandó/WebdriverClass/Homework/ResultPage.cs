﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass
{
    class ResultPage :BasePage
    {
        public ResultPage(IWebDriver webDriver) : base(webDriver)
        {

        }

        public ClickResultWidget ClickButton(IWebDriver webDriver, String clickTo)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            wait.Until(d => d.FindElement(By.ClassName("search-bar-cats")).Displayed);

            webDriver.FindElement(By.ClassName(clickTo)).Click();
            
            return new ClickResultWidget(webDriver);
        }

        public void CreateScreenshot(String testName)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string screenshotName = baseDirectory + testName + "_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + "_error.png";
            Driver.Manage().Window.Maximize();
            var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
            screenshot.SaveAsFile(screenshotName, ScreenshotImageFormat.Png);
        }
    }
}
