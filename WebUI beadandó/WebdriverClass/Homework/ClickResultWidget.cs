﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass
{
    class ClickResultWidget : BasePage
    {
        public ClickResultWidget(IWebDriver driver) : base(driver)
        {

        }

        private IWebElement uadList => Driver.FindElement(By.ClassName("uad-list"));
        private IWebElement cardFilter => Driver.FindElement(By.ClassName("card uad-filter"));
        private IWebElement navBar => Driver.FindElement(By.ClassName("navbar navbar-default pager-navbar justify-content-center justify-content-md-between"));

        private List<IWebElement> listItems => uadList.FindElements(By.ClassName("media")).ToList();

        public int GetListCount()
        {
            return listItems.Count;
        }

    }
}
