﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Xml.Linq;
using OpenQA.Selenium.Chrome;
using WebdriverClass;

namespace WebdriverClass
{
    class BadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource("HomeworkTestData")]
        [Category("Homework")]
        public void HardveraproTest(String page,String className, String expectedField)
        {
            ResultPage resultPage = LinkPage.NavigatToLink(Driver, page).GetResultPage();

            ClickResultWidget clickResultWidget = resultPage.ClickButton(Driver, className);

            try
            {
                Assert.That(clickResultWidget?.GetListCount(), Is.GreaterThan(int.Parse(expectedField)));
            }
            catch (NUnit.Framework.AssertionException)
            {
                string testName = TestContext.CurrentContext.Test.MethodName.ToString();
                resultPage.CreateScreenshot(testName);
            }
        }

        static IEnumerable HomeworkTestData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\Homework\\homework.xml");
            return
                from vars in doc.Descendants("homeworkData")
                let page = vars.Attribute("page").Value
                let className = vars.Attribute("className").Value
                let expectedField = vars.Attribute("expectedField").Value
                select new object[] { page, className, expectedField };
        }
    }
}
