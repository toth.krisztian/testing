﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass
{
    class LinkPage : BasePage
    {
        public LinkPage(IWebDriver webDriver) :base(webDriver)
        {

        }

        public static LinkPage NavigatToLink(IWebDriver webDriver, String link)
        {
            webDriver.Url = link;
            
            return new LinkPage(webDriver);
        }

        public ResultPage GetResultPage()
        {
            return new ResultPage(Driver);
        }
    }
}
